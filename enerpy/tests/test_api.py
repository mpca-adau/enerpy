import logging
import os
from datetime import datetime, timedelta
from pprint import pprint

import pytest
from logger_mixin import LoggerMixin
from pandas import DataFrame

from enerpy import API, EIASeries
from enerpy import BroadCategory, NoResultsError, SeriesTypeError, APIKeyError

EIA_API_KEY = os.environ['EIA_API_KEY']
LoggerMixin.init_logging(logging.DEBUG)


class TestAPI(LoggerMixin):
    api = API(EIA_API_KEY)

    def test_find_updates(self):
        # GDP Current Dollar
        us = self.api.updated_series(datetime.now() - timedelta(2048), starting_category=40829)
        assert len(us) > 50  # should be like 52, but this is safer

        us = self.api.updated_series(datetime.now() - timedelta(2048), starting_category=40829,
                                     series_filter=lambda x: '.MN.' in x)
        assert len(us) == 1

        us = self.api.updated_series(datetime.now() - timedelta(31), starting_category=40203)
        assert len(us) > 0
        assert len(us) < 30000

    def test_search_by_category(self):
        with pytest.raises(APIKeyError):
            api = API('5325235236243234234234')
            api.search_by_category(40924)

        with pytest.raises(NoResultsError):
            self.api.search_by_category(35235342342342)
        results = self.api.search_by_category(40924)
        assert len(results) > 0
        assert list(results.keys())[0].startswith('SEDS.')
        assert isinstance(list(results.values())[0], EIASeries)
        s = results['SEDS.AVTCP.AK.A']
        assert s.msn == 'AVTCP'
        with pytest.raises(BroadCategory):
            self.api.search_by_category(371, 2)
        r = self.api.search_by_category(2101)
        s = list(r.values())[0]
        with pytest.raises(SeriesTypeError):
            print(s.msn)

        r = self.api.search_by_category(41)

        pprint(set(a.frequency for a in r.values()))

    def test_search_by_keyword(self):
        with pytest.raises(NoResultsError):
            self.api.search_by_keyword('mafletnalera')
        results = self.api.search_by_keyword('minnesota', data_set='SEDS')
        assert len(results) > 0
        assert list(results.keys())[0].startswith('SEDS.')
        assert isinstance(list(results.values())[0], EIASeries)

    def test_search_by_date(self):
        with pytest.raises(NoResultsError):
            self.api.search_by_date(datetime(1001, 1, 1), datetime(1002, 1, 1))

        results = self.api.search_by_date(datetime(2015, 1, 1), datetime(2015, 11, 2))
        pprint(set(a.frequency for a in results.values()))
        assert len(results) > 0

        assert isinstance(list(results.values())[0], EIASeries)

    def test_data_by_category(self):
        cat = 475136
        r = self.api.data_by_category(cat)
        assert len(r) > 0
        v = list(r.values())[0]
        assert isinstance(v, EIASeries)
        assert isinstance(v.data_points, DataFrame)
        assert len(v.data_points) > 0

    def test_data_by_keyword(self):
        r = self.api.data_by_keyword(['minnesota', 'net', 'interstate'], data_set='SEDS')
        assert len(r) > 0
        v = list(r.values())[0]
        assert isinstance(v, EIASeries)
        assert isinstance(v.data_points, DataFrame)
        assert len(v.data_points) > 0

    def test_data_by_date(self):
        with pytest.raises(NoResultsError):
            self.api.data_by_date(datetime(1001, 1, 1), datetime(1002, 1, 1))

        results = self.api.data_by_date(datetime(2015, 1, 1), datetime(2015, 2, 2))
        pprint(results)
        pprint(set(a.frequency for a in results.values()))
        assert len(results) > 0

        assert isinstance(list(results.values())[0], EIASeries)
