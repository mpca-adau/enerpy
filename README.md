# EnerPy
[![pipeline status](https://gitlab.com/mpca-adau/enerpy/badges/master/pipeline.svg)](https://gitlab.com/mpca-adau/enerpy/-/commits/master)
[![coverage report](https://gitlab.com/mpca-adau/enerpy/badges/master/coverage.svg)](https://gitlab.com/mpca-adau/enerpy/-/commits/master)

[![PyPI](https://img.shields.io/pypi/v/enerpy?style=flat-square)](https://pypi.org/project/enerpy/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/enerpy?style=flat-square)](https://pypi.org/project/enerpy/)
[![PyPI - License](https://img.shields.io/pypi/l/enerpy?style=flat-square)](https://pypi.org/project/enerpy/)


A Python 3 wrapper for the Energy Information Agency (EIA) API. Designed to
return data formatted for easy loading into pandas DataFrame objects.

## Installation
```sh
pip install enerpy
```


## EIA API
[EIA's API documentation](https://www.eia.gov/opendata/commands.php).


## Authentication

The EIA API uses an API key for authentication. Obtain an EIA API key by registering on
the [EIA website](https://www.eia.gov/opendata/register.php).


## Credits
Forked from https://github.com/mra1385/eia-python
